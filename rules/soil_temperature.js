/*********************************************************
 * Температура почвы
 *********************************************************/

// Изменяемые параметры
var soil_temperature_max = 20, // Максимальная температура почвы
		soil_temperature_min = 18, // Минимальная температура почвы
		soil_temperature_sensor = "wb-ms-thls-v2_5/Temperature", // Датчик температуры почвы в виде "имя датчика/имя параметра"
		soil_heater = "wb-gpio/EXT1_R3A1"; // Реле обогревателя почвы в виде "имя устройства/имя параметра"

// Не изменяемые параметры
var soil_temperature_normal = (soil_temperature_max - soil_temperature_min) / 2; // Вычисляем нормальную температуру почвы

defineRule("soil_temperature", {
	whenChanged: soil_temperature_sensor, // При изменении датчика температуры почвы
	then: function (newValue, devName, cellName) {
		if (newValue < soil_temperature_normal) { // Если температура почвы в теплице меньше нормы
			dev[soil_heater] = 1; // Включаем обогреватель почвы
		} else { // Если температура почвы в норме
			dev[soil_heater] = 0; // Выключаем обогреватель почвы
		}
	}
});
