/*********************************************************
 * Влажность воздуха
 *********************************************************/

// Изменяемые параметры
var air_humidity_max = 45, // Максимальная влажность воздуха
		air_humidity_min = 30, // Минимальная влажность воздуха
		air_humidity_sensor = "wb-ms-thls-v2_5/Humidity", // Датчик влажности воздуха в виде "имя датчика/имя параметра"
		air_dehumidifier = "wb-gpio/EXT1_R3A1", // Реле осушителя воздуха в виде "имя устройства/имя параметра" (вытяжка)
		air_humidifier = "wb-gpio/EXT1_R3A2"; // Реле увлажнителя воздуха в виде "имя устройства/имя параметра" (фоггер)

// Не изменяемые параметры
var air_dehumidifier_active = 0, // Примет 1 если скрипт влажности активирует вытяжку
		air_humidifier_active = 0; // Примет 1 если скрипт влажности активирует фоггер

// Запуск контроля влажности воздуха при изменении датчика
defineRule("air_humidity1", {
	whenChanged: air_humidity_sensor,
	then: function() {
		air_humidity_control(dev[air_humidity_sensor]);
	}
});

// Запуск контроля влажности воздуха при переключении устройств другими правилами
defineRule("air_humidity2", {
	when: function() {
		var changed = 
		// Если фоггер переключило другое правило
		(dev[air_humidifier] == 1 && air_humidifier_active == 0 || dev[air_humidifier] == 0 && air_humidifier_active == 1) || 
		 // Если кулер переключило другое правило
		(dev[air_dehumidifier] == 1 && air_dehumidifier_active == 0 || dev[air_dehumidifier] == 0 && air_dehumidifier_active == 1);
		return changed;
	},
	then: function() {
		log.warning("_humidity_ Устройства переключены извне");
		air_humidity_control(dev[air_humidity_sensor]);
	}
});

// Функция контроля влажности воздуха
function air_humidity_control(sensor) {
	if (sensor > air_humidity_max) { // Если влажность воздуха выше чем нужно
		dev[air_dehumidifier] = 1; // Уменьшаем влажность воздуха
		log("Уменьшаем влажность воздуха");
		air_dehumidifier_active = 1;
		dev[air_humidifier] = 0; // На всякий случай выключаем увлажнитель
	} else if (sensor < air_humidity_min) { // Если влажность воздуха ниже чем нужно
		dev[air_humidifier] = 1; // Увеличиваем влажность вздуха
		log("Увеличиваем влажность вздуха");
		air_humidifier_active = 1;
		dev[air_dehumidifier] = 0; // На всякий случай выключаем осушитель
	} else { // Если значение влажности воздуха в норме выключаем все приборы изменения влажности
		log("Значение влажности в норме");

		if (air_dehumidifier_active) { // Если скрипт влажности активировал вытяжку
			// Выключим вытяжку
			dev[air_dehumidifier] = 0;
			air_dehumidifier_active = 0;
		}

		if(air_humidifier_active) { // Если скрипт влажности активировал фоггер
			// Выключим фоггер
			dev[air_humidifier] = 0; 
			air_humidifier_active = 0;
		}
	}
};
