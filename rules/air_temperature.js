/*********************************************************
 * Температура воздуха
 *********************************************************/

// Изменяемые параметры
var air_temperature_max = 20, // Максимальная температура воздуха в градусах по цельсию
		air_temperature_min = 18, // Минимальная температура воздуха в градусах по цельсию
		air_temperature_sensor = "wb-ms-thls-v2_5/Temperature", // Датчик температуры воздуха в виде "имя датчика/имя параметра"

		air_humidity_max = 45, // Максимальная влажность воздуха
		air_humidity_min = 30, // Минимальная влажность воздуха
		air_humidity_sensor = "wb-ms-thls-v2_5/Humidity", // Датчик влажности воздуха в виде "имя датчика/имя параметра"

		air_cooler_fan = "wb-gpio/EXT1_R3A1", // Реле охладителя воздуха в виде "имя устройства/имя параметра" (Вытяжка)
		air_cooler_fоgger = "wb-gpio/EXT1_R3A2", // Реле охладителя воздуха в виде "имя устройства/имя параметра" (Туманообразователь)
		air_heater =  "wb-gpio/EXT1_R3A3"; // Реле обогревателя воздуха в виде "имя устройства/имя параметра"

// Не изменяемые параметры
var	air_cooler_fan_active = 0, // Примет 1 если скрипт температуры активирует вытяжку
		air_cooler_fоgger_active = 0; // Примет 1 если скрипт температуры активирует фоггер

defineRule("air_temperature1", {
	whenChanged:air_temperature_sensor,
	then: function(){
		air_temperature_control(dev[air_temperature_sensor]);
	}
});

defineRule("air_temperature2", {
	when: function() {
		var changed = 
		// Если фоггер переключило другое правило
		(dev[air_cooler_fоgger] == 1 && air_cooler_fоgger_active == 0 || dev[air_cooler_fоgger] == 0 && air_cooler_fоgger_active == 1) || 
		 // Если кулер переключило другое правило
		(dev[air_cooler_fan] == 1 && air_cooler_fan_active == 0 || dev[air_cooler_fan] == 0 && air_cooler_fan_active == 1);
		return changed;
	},
	then: function() {
		log.warning("_temperature_ Устройства переключены извне");
		air_temperature_control(dev[air_temperature_sensor]);
	}
});

// Функция контроля температуры воздуха
function air_temperature_control(sensor) {
	if (sensor > air_temperature_max) { // Если температура воздуха в теплице больше чем нужно
		if(dev[air_humidity_sensor] < air_humidity_min) { // Если Влажность воздуха низкая
			dev[air_cooler_fоgger] = 1; // Уменьшаем температуру фоггером
			log("Уменьшаем температуру фоггером");
			air_cooler_fоgger_active = 1;
		} else {
			dev[air_cooler_fan] = 1; // Уменьшаем температуру вытяжкой
			log("Уменьшаем температуру вытяжкой");
			air_cooler_fan_active = 1;
		}
		dev[air_heater] = 0; // Выключаем подогреватель
	} else if (sensor < air_temperature_min) { // Если температура воздуха в меньше чем нужно
		dev[air_heater] = 1; // Увеличиваем температуру
		log("Увеличиваем температуру обогревателем");
		dev[air_cooler_fоgger] = 0; // Выключаем фоггер
		dev[air_cooler_fan] = 0; // Выключаем вытяжку
	} else { // Если температура в норме, выключаем все устройства изменения температуры
		log("Температура в норме");
		
		dev[air_heater] = 0;

		if(air_cooler_fоgger_active) { // Если скрипт температуры активировал фоггер
			// Выключим фоггер
			dev[air_cooler_fоgger] = 0;
			air_cooler_fоgger_active = 0;
		}

		if(air_cooler_fan_active) { // Если скрипт температуры активировал вытяжку
			// Выключим вытяжку
			dev[air_cooler_fan] = 0;
			air_cooler_fan_active = 0;
		}
	}
};
