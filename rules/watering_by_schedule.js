/*********************************************************
 * Полив
 *
 * По расписанию
 *********************************************************/

// Изменяемые параметры
var water_volume_liters = 2, // Объём воды в литрах на каждое сопло
		irrigation_efficiency = 48, // Производительность одного сопла системы полива. Измеряется в литры/час
		watering_start = new Date(2017, 1, 16), //Дата начала полива (Отсчёт месяцев начинается с 0)
		watering_end = new Date(2017, 1, 20), // Дата окончания полива (Date(2017, 1, 20) = (20.02.2017))
		watering_time = [18,33],// Время начала полива (12.00). Массив. Формируется автоматически на основе выбранного времени через специальный контрол
		watering_frequency = 5 // Частота полива в днях
		watering_relay = "wb-gpio/EXT1_R3A7"; // Реле системы полива в виде "имя устройства/имя параметра"

// Не изменяемые параметры
var watering_duration = 60 * water_volume_liters / irrigation_efficiency * 60000; // Рассчитываем длительность полива в милисекундах
log("watering_duration: " + watering_duration);

// TODO: поискать решения для конвертации вводимого времени в cron-строку
var cron_string = "0 " + watering_time[1] + " " +  watering_time[0] + " */" + watering_frequency +" * *"; // Формируем строку для cron
log("cron строка: " + cron_string);

var now = new Date(); // Сегодняшняя дата
var today = new Date(now.getFullYear(), now.getMonth(), now.getDate());

// Если дата начала полива не в прошлом
if (today <= watering_start) {
	// Вычисляем время активации правила (количество дней до начала полива)
	var milliseconds = watering_start - today;
	log("Время задержки: " + milliseconds);
	// Устанавливаем задержку выполнения правила 
	setTimeout("watering_by_schedule", milliseconds);
	// Один сеанс полива = запуск скрипта
	defineRule("watering_by_schedule", {
		when: cron(cron_string),
		then: function (newValue, devName, cellName) {
			log("Полив по расписанию запущен");
			var now = new Date();
			var today = new Date(now.getFullYear(), now.getMonth(), now.getDate());
			if(today < watering_end) { // Если период полива ещё не истёк
				log("Полив по расписанию начат");
				dev[watering_relay] = 1; // Включим полив
				setTimeout(
					function(){
						log("Полив по расписанию завершён");
						dev[watering_relay] = 0 // Выключим полив
					},
					watering_duration);
			}
		}
	});
} else {
	log.warning('Дата начала полива в прошлом');
}
