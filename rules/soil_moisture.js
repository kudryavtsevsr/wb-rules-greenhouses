/*********************************************************
 * Влажность почвы
 * 
 * Только мониторинг
 *********************************************************/

// Изменяемые параметры
var soil_moisture_min = 85, // Минимальная влажность почвы
		soil_moisture_max = 90, // Максимальная влажность почвы
		soil_moisture_sensor = "wb-ms-thls-v2_5/Humidity", // Датчик влажности почвы в виде "имя датчика/имя параметра"

// Не изменяемые параметры
var soil_moisture_trigger = null; // При сухости примет 0 при влажности 1

defineRule("soil_moisture", {
	whenChanged: soil_moisture_sensor, // Когда значение датчика влажности почвы изменилось (если датчиков несколько - берём среднее)
	then: function (newValue, devName, cellName) {
		if (newValue < soil_moisture_min) { // Если влажность почвы меньше минимальной
			if(soil_moisture_trigger != 0) {
				log.warning('Почва слишком сухая'); // Сообщаем о сухости почвы один раз
			}
			soil_moisture_trigger = 0;
		} else if (newValue > soil_moisture_max) { // Если влажность почвы выше максимальной
			if(soil_moisture_trigger != 1) {
				log.warning('Почва слишком влажная');  // Сообщаем о повышенной влажности почвы один раз
			}
			soil_moisture_trigger = 1;
		}
	}
});
