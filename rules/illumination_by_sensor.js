/*********************************************************
 * Освещение 
 *
 * По датчику освещённости
 *********************************************************/

// Изменяемые параметры
var illumination_min = 180, // Минимальное допустимое значение освещённости в люксах
		illumination_time_start = [18,12], // Время начала контроля освещения
		illumination_time_end = [20,20], // Время окончания контроля освещения
		illumination_sensor = "wb-ms-thls-v2_5/Illuminance", // Датчик осаещённости в виде "имя устройства/имя параметра"
		illumination = "wb-gpio/EXT1_R3A7"; // Реле освещения в виде "имя устройства/имя параметра"

// Не изменяемые параметры
var illumination_delay = 30 * 1000, // Задержка включения/отключения датчика освещёности
		illumination_on_timer = null, // Таймер включения освещения
		illumination_off_timer = null; // Таймер выключения освещения

// Запуск контроля освещения при изменении датчика освещённости
defineRule("illumination_by_sensor1", {
	whenChanged: illumination_sensor,
	then: function() {
		illumination_control(dev[illumination_sensor]);
	}
});

// Запуск контроля освещения при наступлении времени начала контроля освещения
defineRule("illumination_by_sensor2", {
	when: cron("0 " + illumination_time_start[1] + " " + illumination_time_start[0] + " * * *"),
	then: function() {
		illumination_control(dev[illumination_sensor]);
	}
});

// Запуск контроля освещения при наступлении времени окончания контроля освещения
defineRule("illumination_by_sensor3", {
	when: cron("0 " + illumination_time_end[1] + " " + illumination_time_end[0] + " * * *"),
	then: function() {
		illumination_control(dev[illumination_sensor]);
	}
});

// Функция контроля освещения
function illumination_control(sensor) {
	var now = new Date(), // Сегодняшняя дата
			today = new Date(now.getFullYear(), now.getMonth(), now.getDate(), now.getHours(), now.getMinutes()); // Дата с часами и минутами
			illumination_time_start_full = new Date(now.getFullYear(), now.getMonth(), now.getDate(), illumination_time_start[0], illumination_time_start[1]);
			illumination_time_end_full = new Date(now.getFullYear(), now.getMonth(), now.getDate(), illumination_time_end[0], illumination_time_end[1]);

	if(today > illumination_time_start_full && today < illumination_time_end_full) { // Если чейчас время контроля освещённости

		if (sensor < illumination_min) { // Если освещённость ниже чем нужно

			if(illumination_off_timer){
				illumination_off_timer = clearTimeout(illumination_off_timer); // Очищаем таймер выключения
				log("Таймер выключения очищен");
			}

			if (!dev[illumination] && !illumination_on_timer) { // Если освещение выключено и таймер включения ещё не запущен
				log("Таймер включения запущен");
				illumination_on_timer = setTimeout(function(){ // Запускаем таймер включения
					dev[illumination] = 1; // Включаем освещение
					log("Освещение включено");
				}, illumination_delay);
			}

		} else { // Если освещённость в норме

			if(illumination_on_timer){
				illumination_on_timer = clearTimeout(illumination_on_timer); // Очищаем таймер включения
				log("Таймер включения очищен");
			}
	
			if (dev[illumination] && !illumination_off_timer) { // Если освещение включено и таймер выключения ещё не запущен
				log("Таймер выключения запущен");
				illumination_off_timer = setTimeout(function(){
					dev[illumination] = 0; // Выключаем освещение
					log("Освещение выключено");
				}, illumination_delay);
			}

		}

	} else { // Если сейчас не время контроля освещения
		dev[illumination] = 0; // Выключаем освещение
	}
};
