/*********************************************************
 * Полив
 *
 * По датчику влажности
 *********************************************************/

// Изменяемые параметры
var soil_moisture_min = 200, // Минимальная влажность почвы
		soil_moisture_max = 700, // Максимальная влажность почвы
		soil_moisture_sensor = "wb-ms-thls-v2_5/Humidity", // Датчик влажности почвы в виде "имя датчика/имя параметра"
		soil_moisture_relay = "wb-gpio/EXT1_R3A7"; // Реле системы полива в виде "имя устройства/имя параметра"

// Не изменяемые параметры
var soil_moisture_normal = (soil_moisture_max - soil_moisture_min) / 2; // Вычисляем нормальную влажность почвы

defineRule("watering_by_sensor", {
	whenChanged: soil_moisture_sensor, // Когда значение датчика влажности почвы изменилось
	then: function (newValue, devName, cellName) {
		if (newValue < soil_moisture_normal) { // Если влажность почвы меньше нормы
			dev[soil_moisture_relay] = 1; // Поливаем
		} else { // Если влажность почвы выше нормы
			dev[soil_moisture_relay] = 0; // Прекращаем полив
		}
	}
});
